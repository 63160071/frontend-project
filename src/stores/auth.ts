import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
    //return authName.value !== "";
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;

    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      //console.log("Successsssss !!!!");
      router.push("/");
    } catch (e) {
      console.log(e);
      messageStore.showError("Username หรือ password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
    //console.log(res);
  };
  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin };
});
